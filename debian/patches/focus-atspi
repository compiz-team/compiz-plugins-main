https://gitlab.com/compiz/compiz-plugins-main/-/merge_requests/84

commit b4438cff73371548c35dfc025c1de571e8325563
Author: Samuel Thibault <samuel.thibault@ens-lyon.org>
Date:   Mon Jul 6 22:24:34 2020 +0200

    focuspoll: Fix disabling a11ywatcher on disabling focus poll
    
    focuspollRemoveFocusPolling would previously return as soon as it finds the
    poller, and thus never actually check for fs->clients being NULL.
    
    Actually, we do not want to make focuspollRemoveFocusPolling call
    compRemoveTimeout itself, since it is typically called from the timeout
    handler itself, and handleTimeouts doesn't support removing the timeout
    while it is getting processed:
    
    compRemoveTimeout (display.c:1016)
    focuspollRemoveFocusPolling(_CompScreen*, int) (focuspoll.cpp:216)
    updateFocusInterval (ezoom.c:1538)
    updatePosition(void*) (focuspoll.cpp:124)
    handleTimeouts (display.c:1367)
    eventLoop (display.c:1750)
    main (main.c:522)
    
    We can as well check for fs->clients being NULL and simply return FALSE to
    disable the timeout after looping over clients inside updatePosition.

diff --git a/src/focuspoll/focuspoll.cpp b/src/focuspoll/focuspoll.cpp
index 88d73f5..389d783 100644
--- a/src/focuspoll/focuspoll.cpp
+++ b/src/focuspoll/focuspoll.cpp
@@ -132,6 +132,13 @@ updatePosition (void *c)
     }
 
     fs->a11ywatcher->resetFocusQueue ();
+
+    if (!fs->clients)
+    {
+	fs->a11ywatcher->setActive (false);
+	fs->updateHandle = 0;
+	return FALSE;
+    }
     return TRUE;
 }
 
@@ -205,13 +212,6 @@ focuspollRemoveFocusPolling (CompScreen            *s,
 	    free (fc);
 	    return;
 	}
-
-    if (!fs->clients && fs->updateHandle)
-    {
-	fs->a11ywatcher->setActive (false);
-	compRemoveTimeout (fs->updateHandle);
-	fs->updateHandle = 0;
-    }
 }
 
 static CompSize
commit 5669adcc566234b5581fb444e6870fbda0b5419a
Author: Samuel Thibault <samuel.thibault@ens-lyon.org>
Date:   Mon Jul 6 22:29:10 2020 +0200

    focuspoll: do not enable at-spi watches by default
    
    We already properly enable it whenever we actually need it.

diff --git a/src/focuspoll/accessibilitywatcher.cpp b/src/focuspoll/accessibilitywatcher.cpp
index 0df50c9..9952755 100644
--- a/src/focuspoll/accessibilitywatcher.cpp
+++ b/src/focuspoll/accessibilitywatcher.cpp
@@ -368,8 +368,6 @@ AccessibilityWatcher::AccessibilityWatcher () :
     windowCreateListener = atspi_event_listener_new (reinterpret_cast <AtspiEventListenerCB> (onWindowCreate), this, NULL);
     descendantChangedListener = atspi_event_listener_new (reinterpret_cast <AtspiEventListenerCB> (onDescendantChanged), this, NULL);
     readingListener = atspi_event_listener_new (reinterpret_cast <AtspiEventListenerCB> (onReading), this, NULL);
-
-    addWatches ();
 }
 
 AccessibilityWatcher::~AccessibilityWatcher ()
